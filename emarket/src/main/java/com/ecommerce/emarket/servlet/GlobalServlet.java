package com.ecommerce.emarket.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.ecommerce.emarket.dao.CategoryDao;
import com.ecommerce.emarket.dao.ProductDao;
import com.ecommerce.emarket.dao.UserDao;
import com.ecommerce.emarket.model.Category;
import com.ecommerce.emarket.model.Product;
import com.ecommerce.emarket.model.User;

import javax.servlet.annotation.WebServlet;
/**
 * Servlet implementation class GlobalServlet
 */
@WebServlet("/")
public class GlobalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ProductDao prodDao;
	private UserDao userDao;
	private CategoryDao categoryDao;
	
	private Gson gson = new Gson();

   public void init() throws ServletException {
		prodDao = new ProductDao();
        userDao = new UserDao();
        categoryDao = new CategoryDao();
	}
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		
		switch(action){
		case "/show-products":
			try {
				showProducts(request,response);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
			
		case "/show-products-by-id":
			try {
				showProductById(request,response);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		
	   case "/show-users":
		   try {
			showUser(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
		
	case "/show-users-by-id":
		try {
			showUserById(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
		
	case "/show-category":
		   try {
			showCategory(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
		
	case "/show-products-by-category":
		try {
			showProductByCategory(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
	}
					
  }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		
		switch(action){
		case "/add-products":
			try {
				addProducts(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
			
		
	case "/add-users":
		try {
			addUser(request,response);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
		
	case "/add-category":
		try {
			addCategory(request,response);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
	}
		
  }

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getServletPath();
		
		switch(action){
		case "/edit-products":
			try {
				editProducts(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		
	case "/edit-users":
		try {
			editUser(request,response);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
	}
  }

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		
		switch(action){
		case "/delete-products":
			try {
				deleteProducts(request,response);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
	
		
	case "/delete-users":
		try {
			deleteUser(request,response);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		break;
	}

 }
	
	private void showProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		try {
			PrintWriter out = response.getWriter();
			List<Product> products = prodDao.getAllProducts();
			String prodJSON = gson.toJson(products);
			out.print(prodJSON);
			out.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void showProductById(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		long prod_id = Long.parseLong(request.getParameter("prod_id"));
		try {
			PrintWriter out = response.getWriter();
			Product existingProd = prodDao.getProductById(prod_id);
			String prodJSON = gson.toJson(existingProd);
			out.print(prodJSON);
			out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addProducts(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String requestBody = new BufferedReader(request.getReader()).lines().collect(Collectors.joining(System.lineSeparator()));
		Gson gson = new Gson();
		Product newProd = gson.fromJson(requestBody, Product.class);
		prodDao.addProducts(newProd);
	}
	
	private void editProducts(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String requestBody = new BufferedReader(request.getReader()).lines().collect(Collectors.joining(System.lineSeparator()));
		System.out.println(requestBody);
		Gson gson = new Gson();
		Product newProd = gson.fromJson(requestBody, Product.class);
		prodDao.updateProducts(newProd);
	}
	
	private void deleteProducts(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		long prod_id = Long.parseLong(request.getParameter("prod_id"));
		try {
			prodDao.deleteProductById(prod_id);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void showUser(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		try {
			PrintWriter out = response.getWriter();
			List<User> users = userDao.getAllUser();
			String prodJSON = gson.toJson(users);
			out.print(prodJSON);
			out.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void showUserById(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		String user_id = request.getParameter("user_id");
		try {
			PrintWriter out = response.getWriter();
			User existingUser = userDao.getUserById(user_id);
			String prodJSON = gson.toJson(existingUser);
			out.print(prodJSON);
			out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String requestBody = new BufferedReader(request.getReader()).lines().collect(Collectors.joining(System.lineSeparator()));
		Gson gson = new Gson();
		User newUser = gson.fromJson(requestBody, User.class);
		userDao.addUser(newUser);
	}
	
	private void editUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String requestBody = new BufferedReader(request.getReader()).lines().collect(Collectors.joining(System.lineSeparator()));
		System.out.println(requestBody);
		Gson gson = new Gson();
		User newUser = gson.fromJson(requestBody, User.class);
		userDao.updateUser(newUser);
	}
	
	private void deleteUser(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String user_id = request.getParameter("user_id");
		try {
			userDao.deleteUserById(user_id);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void showCategory(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		try {
			PrintWriter out = response.getWriter();
			List<Category> category = categoryDao.getAllCategory();
			String prodJSON = gson.toJson(category);
			out.print(prodJSON);
			out.flush();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void addCategory(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String requestBody = new BufferedReader(request.getReader()).lines().collect(Collectors.joining(System.lineSeparator()));
		Gson gson = new Gson();
		Category newCat = gson.fromJson(requestBody, Category.class);
		categoryDao.addCategory(newCat);
	}
	
	private void showProductByCategory(HttpServletRequest request, HttpServletResponse response) throws ServletException,SQLException, IOException {
		String name = request.getParameter("cat_name");
		try {
			PrintWriter out = response.getWriter();
			Category existingUser = categoryDao.getProductByCategory(name);
			String prodJSON = gson.toJson(existingUser);
			out.print(prodJSON);
			out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
