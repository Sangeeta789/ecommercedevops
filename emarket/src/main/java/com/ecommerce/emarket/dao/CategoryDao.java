package com.ecommerce.emarket.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ecommerce.emarket.model.Category;
import com.ecommerce.emarket.utility.CategoryUtility;
import com.ecommerce.emarket.utility.ProductUtility;



public class CategoryDao {

	public void addCategory(Category category) {
		SessionFactory sf = CategoryUtility.getSessionFactory();
		Session ss = sf.openSession();
		Transaction tr = ss.beginTransaction();
		ss.save(category);
		tr.commit();
		ss.close();
	}
	
	public List<Category> getAllCategory(){
		SessionFactory sf = CategoryUtility.getSessionFactory();	
		Session ss = sf.openSession();
		return ss.createQuery("from Category",Category.class).getResultList();
	}
	
	public Category getProductByCategory(String cat_name) {
		   SessionFactory sessionFactory = ProductUtility.getSessionFactory();
		   Session session = sessionFactory.openSession();
		   return session.find(Category.class, cat_name);
		 }
}
