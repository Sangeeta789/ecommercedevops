package com.ecommerce.emarket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_details")
public class User {

	@Id
	@Column(name="user_id")
	private String id;
	
	@Column(name="user_name")
	private String name;
	
	@Column(name="phone_no")
	private int no;
	
	@Column(name="address")
	private String address;
	
	@Column(name="user_password")
	private String ps;
	
    public User() {}
	
	public User(String id, String name, int no, String address, String ps) {
		super();
		this.id = id;
		this.name = name;
		this.no = no;
		this.address = address;
		this.ps = ps;
	}
	
	
	public User( String name, int no, String address, String ps) {
		super();
		this.name = name;
		this.no = no;
		this.address = address;
		this.ps = ps;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getno() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return ps;
	}

	public void setPassword(String ps) {
		this.ps = ps;
	}
	

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", phone_no=" + no + ", address=" + address + ", password=" + ps
				+ "]";
	}    
    


	
	
	
}
