package com.ecommerce.emarket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "productdetails")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prod_id")
	private long id;
	
	@Column(name="prod_name")
	private String name;
	
	@Column(name="prod_desc")
	private String desc;
	
	@Column(name="prod_price")
	private double price;
	
	@Column(name="quantity")
	private int qty;
	
	@Column(name="prod_cat")
	private String category;
	
	public Product() {}
	
	public Product(String name, String desc, double price, int qty, String category) {
		super();
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
		this.category = category;
	}
	
	
	public Product(long id, String name, String desc, double price, int qty, String category) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
		this.category = category;
	}
	
	public Product(long id, String name, String desc, double price, int qty) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.price = price;
		this.qty = qty;
	}
	
	public Product(String category) {
		super();
		this.category = category;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", desc=" + desc + ", price=" + price + ", qty=" + qty
				+ ", category=" + category + "]";
	}
	
	
	
}
